#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>

#include "mstd/mstd.h"
#include "mstd/mstdio.h"

/// --- File I/O ---

MFILE *mfopen(const str path) {
  MFILE *file = new MFILE[sizeof(MFILE)];
  file->path = path;
  file->cfile = fopen(path, "w+");
  return file;
}

MFILE *mfnew(FILE **ptr) {
  MFILE *file = new MFILE[sizeof(MFILE)];
  file->cfile = *ptr;
	return file;  
}

void mfclose(MFILE *file) {
  if (file != NULL) {
    fclose(file->cfile);    
    delete file;
  }
}

bool mfwrite(void *data, size_t size, MFILE *file) {
  FILE *ptr = file->cfile;
  int result = 0;

  if (ptr != NULL) {
    result = fwrite(data, size, 1, ptr);
  }

  fflush(ptr);
  return result;
}

void mfread(str string, size_t size, MFILE *file) {
  FILE *ptr = file->cfile;

  if (ptr != NULL) {
    fgets(string, size, ptr);
  }

  fflush(ptr);
}

bool mfexists(MFILE *file) {
  struct stat st = {0};
  bool exists;

  if (stat(file->path, &st) == -1) exists = false;
  else exists = true;

  return exists;
}

/// --- Utilities ---

str boolstr(bool b) {
	str result;
  if (b) {
    result = new char[5];
    strncpy(result, "true", 5);
  } else { 
    result = new char[6];
    strncpy(result, "false", 6);
  }

	return result;
}

/// --- Processes --- 

PROCESS *newprocess(str exec, str *args) {
  PROCESS *p = new PROCESS;

  p->program = exec;
  p->arguments = args;
  p->ptr = popen(exec, "r+");

  return p;
}

int executeprocess(PROCESS *p) {
  return execv(p->program, p->arguments);
}
