#include "mstd/extra/mbloat.h"
#include "mstd/mstdio.h"

void hello(void) {
  printf("Hello, world!\n");
}

void println(const char *s) {
  printf("%s\n", s);
}
