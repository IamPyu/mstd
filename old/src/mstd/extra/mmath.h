#ifndef MMATH_H
#define MMATH_H

#include "../mstdtypes.h"
#include <math.h>

static const f64 PI_F64 = 3.14159265358979323846;
static const f32 PI_F32 = 3.14159265358979323846f;

static const f64 TAU_F64 = PI_F64 * 2.0;
static const f32 TAU_32 = PI_F32 * 2.0f;

#endif // MMATH_H