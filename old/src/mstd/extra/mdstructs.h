#ifndef MDSTRUCTS_H
#define MDSTRUCTS_H

#include "../mstdtypes.h"
#include <stddef.h>
#include <stdlib.h>

// Creates a new linked list structure with type T
#define MakeLinkedNode(T, name)											\
  struct name##_LINKED_NODE {											\
    T value;															\
    struct name##_LINKED_NODE *next;									\
  };																	\


// Creates a new node named NAME from custom linked list type T with value VAL
#define CreateNode(T, name, val)								\
  T * name = new T;									\
  name ->value = val;														\
  name ->next = NULL;

// Inserts NODE at the HEAD or start of a linked list
#define InsertNodeHead(head, node)							\
  node->next = *head;														\
  *head = node;																	\

// Inserts NODE of type T at the end of linked list starting at HEAD
#define InsertNode(T, head, node)								\
  T *ptr = head;																\
  while (ptr->next != NULL) {										\
    ptr = ptr->next;														\
  }																							\
  ptr->next = node;

#endif // MDSTRUCTS_H
