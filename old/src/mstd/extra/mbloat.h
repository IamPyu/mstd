// MBLOAT_H - Unneccasary bloat and quality of life functions.

#ifndef MBLOAT_H
#define MBLOAT_H

#include "../mstdtypes.h"

// Hello, world!
void hello(void);

// Print a string
void print(const str);
// Print a string include a newline
void println(const str);

#endif // MBLOAT_H
