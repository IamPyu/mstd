#ifndef MSTD_H
#define MSTD_H

#include <stdio.h>

#include "mstdtypes.h"
#include "mstdio.h"
#include "macros.h"


/// --- Utilites ---

// Convert a boolean to a string
str boolstr(bool b);

/// --- Processes ---

struct PROCESS {
  str program;
  str *arguments;
  FILE *ptr;
};

PROCESS *newprocess(str exec, str *args);
int executeprocess(PROCESS *p);

#endif // MSTD_H
