#ifndef MSTDYPES_H
#define MSTDYPES_H

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>

// Evalutes to 1
#define true 1

// Evaluates to 0
#define false 0

// A str is a pointer to a character, aka array of characters.
#define str char*

// A bool(ean) is either 1(True) or 0(False), 
#define bool _Bool

// A dynptr or void pointer is a pointer that can be substituted for any other type of pointer. You can create a minimal style of generics with these.
#define dynptr void*

// Unsigned byte
typedef uint8_t u8;
// Unsigned 16-bit Integer
typedef uint16_t u16;
// Unsigned 32-bit Integer
typedef uint32_t u32;
// Unsigned 64-bit Integer
typedef uint64_t u64;
// Unsigned Integer to match machine
typedef long unsigned int usize;

// Signed byte
typedef int8_t i8;
// Signed 16-bit Integer
typedef int16_t i16;
// Signed 32-bit Integer
typedef int32_t i32;
// Signed 64-bit Integer
typedef int64_t i64;
// Signed Integer to match machine
typedef long int isize;

// 32-bit floating point number
typedef float f32;
// 64-bit floating point number
typedef double f64;

#endif // MSTDYPES_H
