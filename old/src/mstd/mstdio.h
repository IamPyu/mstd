#ifndef MSTDIO_H
#define MSTDIO_H

#include <stdio.h>
#include "mstdtypes.h"

// A file structure, create one using mfopen.
struct MFILE {
  const str path;
  FILE *cfile;
};

// Create new MFILE structure
MFILE *mfopen(const str path);

// Creates a new MFILE structure from libc FILE
MFILE *mfnew(FILE **ptr);

// Close FILE
void mfclose(MFILE *file);

// Write DATA to FILE
bool mfwrite(void *data, size_t size, MFILE *file);

// Read SIZE bytes from FILE to STRING
void mfread(str string, size_t size, MFILE *file);

// Check if FILE exists
bool mfexists(MFILE *file);

#endif // MSTDIO_H
