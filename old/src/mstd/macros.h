#ifndef MACROS_H
#define MACROS_H


/// Helper Macros, lots of these are from: https://www.youtube.com/watch?v=6_AvIAlKhG8

/*
Simple function definition for header files. Example: define(int, hello, (int x, int y))
*/
#define define(type, name, args) type name args;

#if !defined(AssertError)
// Use default AssertError
#define AssertError() (*(int *)0 = 0);
#endif

/*
Assert macro, if test fails you should get a warning looking like this if you didnt define your own custom assert error:
warning: indirection of non-volatile null pointer will be deleted, not trap [-Wnull-dereference]
*/
#define Assert(e) if (!(e)) {AssertError();}

// Stringify macro, converts S to a string
#define Stringify(s) #s

// Merges to expressions together
#define Glue(lhs, rhs) lhs##rhs

// Nullifier
#define Null(T) (T)NULL

#endif // MACROS_H
