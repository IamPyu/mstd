import os
import pathlib

for path in pathlib.Path('./').glob('*.cpp'):
    path = path.name
    out = path.removesuffix('.cpp')
    failure = os.system(f'clang++ -Wall {path} -lmstd -o {out}.out')
    if failure == 0:
        print(f"Compiled {out}!")
    else:
        print(f"Failed to compile {out}!")
