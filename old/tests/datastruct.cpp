#include <mstd/extra/mdstructs.h>
#include <stdio.h>

int main(void) {
  MakeLinkedNode(int, COOL);

  COOL_LINKED_NODE *head = nullptr;
  
  for (int i = 0; i < 5; i++) {
    CreateNode(COOL_LINKED_NODE, tmp, i);
    InsertNodeHead(&head, tmp);
  }

  COOL_LINKED_NODE *tmp = head;

  while (tmp->value != (int)NULL) {
    printf("%d\n", tmp->value);
    COOL_LINKED_NODE *boi = tmp->next;
    delete tmp; // free the memory
    tmp = boi;
  }
  
  return 0;
}
