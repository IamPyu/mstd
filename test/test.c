#define MSTD_STDIO_IMPLEMENTATION
#include <mstd/mstd_stdio.h>

#define BASE_MACROS_IMPLEMENTATION
#include <base/base_macros.h>
#define BASE_LIST_IMPLEMENTATION
#include <base/base_list.h>

static const int COUNT = 15;

int main() {
  CreateLinkedNode(int, hi);

  typedef struct hi_linked_node hln_t;

  InitLinkedNode(hln_t, HEAD, 0);

  for (int i = 0; i < COUNT; i++) {
	InitLinkedNode(hln_t, n, i + 1);
	InsertNode(hln_t, HEAD, n);
  }

  hln_t *ptr = HEAD;
  while (ptr != NULL) {
	if (ptr->value != 0) {
	  printf("%d - ", ptr->value);
	}
	
	ptr = ptr->next;
  }
  
  printf("\n");
  
  return 0;
}
