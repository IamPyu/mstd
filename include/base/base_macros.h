#ifndef BASE_MACROS_H
#define BASE_MACROS_H

#ifdef BASE_MACROS_IMPLEMENTATION


// --- Helper Macros ---

/*
  Quickly define functions in headers where:
  - T is the function return type
  - n is the function name
  - a are the function arguments

  Example:

  Define(int, greet, (const char *name, unsigned int times))
*/
#define Define(t, n, a) t n a ;

#if !defined(AssertError)
// Use default AssertError
#define AssertError() (*(int*)0 = 0);
#endif

/*
  Assert macro checks if X is evaluates true where X is a boolean expression.
  If X is false you will get a warning:

  warning: indirection of non-volatile null pointer will be deleted, not trap [-Wnull-dereference]

  You can also define AssertError to create a custom way to handle assert errors.
*/
#define Assert(e) if (!(e)) {AssertError();}

// Converts S to a string.
#define Stringify(s) #s

// Merges tokens: lhs, and rhs.
#define Glue(lhs, rhs) lhs##rhs

// --- Operating System macros ---

#if defined(__ANDROID__)
#define OS_ANDROID 1

#elif defined(AMIGA)
#define OS_AMIGA 1

#elif defined(__BEOS__)
#define OS_BEOS 1

#elif defined(__FreeBSD__) || defined(__NetBSD__) || defined(__OpenBSD__) \
  || defined(__DragonFly__) || defined(__bsdi__)
#define OS_BSD 1

#elif defined(__GNU__) && defined(__gnu_hurd__)
#define OS_GNUHURD 1

#elif defined(__GNU__) && defined(__gnu_linux__)
#define OS_GNULINUX 1

#elif defined(__linux__)
#define OS_LINUX 1

#elif defined(__APPLE__) && defined(__MACH__)
#define OS_DARWIN 1

#elif defined(macintosh) || defined(Macintosh)
#define OS_MACINTOSH 1

#elif defined(__minix)
#define OS_MINIX 1

#elif defined(MSDOS) || defined(__MSDOS__) || defined(_MSDOS) || defined(__DOS__)
#define OS_MSDOS 1

#elif defined(EPLAN9)
#define OS_PLAN9 1

#elif defined(sun) || defined(__sun)
#define OS_SOLARIS 1

#elif defined(__SYMBIAN32__)
#define OS_SYMBIAN 1

#elif defined(__unix__) || defined(__unix)
#define OS_UNIX 1

#elif defined(_WIN16) || defined(_WIN32) || defined(__WIN32__) || defined(_WIN64) \
  defined(__TOS_WIN__) || defined(__WINDOWS__)
#define OS_WINDOWS 1

#endif

// --- Architecture macros ---

#if defined(__amd64__) || defined(__amd64) || defined(__x86_64__) || defined(__x86_64)
#define ARCH_x86_64 1

#elif defined(__arm__) || defined(__thumb__)
#define ARCH_ARM 1

#elif defined(__aarch64__)
#define ARCH_ARM64 1

#elif defined(i386) || defined(__i386) || defined(__i386__)
#define ARCH_I386 1

#elif defined(__powerpc) || defined(__powerpc__) || defined(__powerpc64__) || defined(__POWERPC__) \
  || defined(__ppc__) || defined(__ppc64__) || defined(__PPC__) || defined(__ARCH_PPC) \
  || defined(_ARCH__PPC64)
#define ARCH_POWERPC 1

#endif


// --- Compiler Macros ---

#if defined(__GNUC__)
#define COMPILER_GNU 1

#elif defined(__clang__)
#define COMPILER_CLANG 1

#elif defined(_MSC_VER)
#define COMPILER_MSCV 1

#elif defined(__MINGW32__) || defined(__MINGW64__)
#define COMPILER_MINGW 1

#endif
#endif

#endif // BASE_MACROS_H
