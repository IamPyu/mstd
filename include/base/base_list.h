#ifndef BASE_LIST_H
#define BASE_LIST_H

#ifdef BASE_LIST_IMPLEMENTATION

#define CreateLinkedNode(T, name)					\
  struct name##_linked_node {					\
	T value;									\
	struct name##_linked_node *next;			\
  }

#define InitLinkedNode(T, name, val)			\
  T * name = ( T *)malloc(sizeof( T ));			\
  name ->value = val;							\
  name ->next = NULL;

// Inserts NODE at the HEAD or start of a linked list
#define InsertNodeHead(head, node)										\
  node->next = *head;													\
  *head = node;															\

// Inserts NODE of type T at the end of linked list starting at HEAD
#define InsertNode(T, head, node)										\
  T *ptr = head;														\
  while (ptr->next != NULL) {											\
    ptr = ptr->next;													\
  }																		\
  ptr->next = node;

#endif

#endif // BASE_LIST_H
