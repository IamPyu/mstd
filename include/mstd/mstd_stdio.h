#ifndef MSTD_STDIO
#define MSTD_STDIO

#ifdef MSTD_STDIO_IMPLEMENTATION

#include <stdio.h>
#include <stdlib.h>

struct mstd_stdio_file {
  FILE *fptr;
}; typedef struct mstd_stdio_file mstd_stdio_file_t;

// Open a file creating a mstd_stdio_file structure.
static mstd_stdio_file_t *mstd_stdio_open(const char *path, const char *mode) {
  mstd_stdio_file_t *ptr = malloc((sizeof(mstd_stdio_file_t)));

  FILE *fptr;
  fptr = fopen(path, mode);

  ptr->fptr = fptr;
  return ptr;
}

// Close a file.
static void mstd_stdio_close(mstd_stdio_file_t *file) {
  fclose(file->fptr);
  free((void*)file);
}

// Write data to a file.
static void mstd_stdio_write(mstd_stdio_file_t *file, void* contents, size_t size) {
  fwrite(contents, size, 1, file->fptr);
  fflush(file->fptr);
}

// Write changes to file.
static void mstdio_stdio_flush(mstd_stdio_file_t *file) {
  fflush(file->fptr);
}

// Read data from file.
static void mstd_stdio_read(mstd_stdio_file_t *file, char *str, size_t size) {
  fgets(str, size, file->fptr);
  fflush(file->fptr);
}


// Macros for standard-(input, output, error)

#define mstd_stdio_getstdin() mstd_stdio_open("/dev/stdin", "r")
#define mstd_stdio_getstdout() mstd_stdio_open("/dev/stdout", "w")
#define mstd_stdio_getstderr() mstd_stdio_open("/dev/stderr", "w")

#endif

#endif // MSTD_STDIO
